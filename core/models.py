from django.db import models
from db.connect_to_db import ConnectDB

STATUS = (('inactive', 'Inactive'),
          ('active', 'Active'))


class User(models.Model):
    @classmethod
    def add_user(cls, **kwargs):
        procedure_name = 'add_user'
        values = (kwargs.get('name'), kwargs.get('email'), kwargs.get('phone'),
                  kwargs.get('mobile_phone'), kwargs.get('status'))
        ConnectDB().execute_procedure(procedure_name, values)

    @classmethod
    def get_all_users(cls):
        list_users = ConnectDB().select('SELECT * FROM get_all_users()')
        return list_users

    @classmethod
    def get_user(cls, user_id):
        user = ConnectDB().select_user('SELECT * FROM get_user(' + user_id + ')')
        return user

    @classmethod
    def edit_user(cls, user_id, **kwargs):
        procedure_name = 'edit_user'
        values = (user_id, kwargs.get('name'), kwargs.get('email'), kwargs.get('phone'),
                  kwargs.get('mobile_phone'), kwargs.get('status'))
        ConnectDB().execute_procedure(procedure_name, values)

    @classmethod
    def filter_by_name(cls, name):
        users = ConnectDB().select("SELECT * FROM get_user_by_name('" + name + "')")
        print users
        return users

    @classmethod
    def delete_user(cls, user_id):
        procedure_name = 'delete_user'
        ConnectDB().execute_procedure(procedure_name, user_id)

    @classmethod
    def get_courses_for_user(cls, user_id):
        sql = 'SELECT * FROM get_user_courses(' + user_id + ')'
        list_courses = ConnectDB().select(sql)
        return list_courses

    @classmethod
    def update_user_courses(cls, user_id, course_codes):
        old_courses = cls.get_courses_for_user(user_id)
        for old_course in old_courses:
            if old_course[0] not in course_codes:
                procedure_name = 'delete_course_for_user'
                ConnectDB().execute_procedure(procedure_name, (user_id, old_course[0]))
            else:
                course_codes.remove(old_course[0])
        procedure_name = 'add_user_courses'
        for course_code in course_codes:
            print course_code
            values = (user_id, course_code)
            ConnectDB().execute_procedure(procedure_name, values)

    @classmethod
    def get_all_courses(cls):
        list_courses = ConnectDB().select('SELECT * FROM get_all_courses()')
        return list_courses
