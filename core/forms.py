from django import forms
from models import STATUS
import re
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Button
from models import User


class UserForm(forms.Form):
    name = forms.CharField()
    email = forms.EmailField()
    phone = forms.CharField(required=False, help_text='Format +xx(xxx)xxxxxxx')
    mobile_phone = forms.CharField(required=False, help_text='Format +xx(xxx)xxxxxxx')
    status = forms.ChoiceField(choices=STATUS, required=False)
    button_name = 'Create'

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', self.button_name))

    def clean_name(self):
        if self.has_digit(self.cleaned_data['name']):
            raise forms.ValidationError('Name cannot contain any digit!')
        return self.cleaned_data['name']

    @staticmethod
    def has_digit(name):
        return bool(re.search(r'\d', name))


class EditUserForm(UserForm):
    name = forms.CharField(disabled=True)
    courses = User.get_all_courses()
    course = forms.ChoiceField(choices=courses, required=False)
    button_name = 'Save'

    def __init__(self, *args, **kwargs):
        super(EditUserForm, self).__init__(*args, **kwargs)
        self.helper.add_input(Button('add', '+', css_class='button-add-course'))
