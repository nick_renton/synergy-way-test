from django.conf.urls import url
from views import index, create_user, edit_user, delete_user, courses

urlpatterns = (
    url(r'^$', index, name='index'),
    url(r'^user/create/$', create_user, name='create_user'),
    url(r'^user/edit/(?P<pk>\d+)/$', edit_user, name='edit_user'),
    url(r'^user/delete/(?P<pk>\d+)/$', delete_user, name='delete_user'),
    url(r'^courses/$', courses, name='courses'),
)
