from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse_lazy
from .models import User
from .forms import UserForm, EditUserForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def index(request):
    filter_name = request.GET.get('filter_by_name')
    if filter_name:
        users_all = User.filter_by_name(str(filter_name))
    else:
        filter_name = ''
        users_all = User.get_all_users()
    items_number = [10, 15, 20]
    number = request.GET.get('number', items_number[0])
    paginator = Paginator(users_all, number)
    page = request.GET.get('page')
    try:
        users_page = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        users_page = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver last page of results.
        users_page = paginator.page(paginator.num_pages)
    return render(request, 'index.html', {'users': users_page, 'number': number, 'items_number': items_number,
                                          'filter_name': filter_name})


def create_user(request):
    form = UserForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            User.add_user(**form.cleaned_data)
            return render(request, 'create_user.html', {'form': form, 'message': 'User created successfully'})
    return render(request, 'create_user.html', {'form': form})


def edit_user(request, pk):
    user = User.get_user(pk)
    form = EditUserForm(request.POST or None, initial=user)
    user_courses = User.get_courses_for_user(pk)
    print user_courses
    if request.method == 'POST':
        if form.is_valid():
            User.edit_user(pk, **form.cleaned_data)
            User.update_user_courses(pk, request.POST.getlist('selected_courses'))
            user_courses = User.get_courses_for_user(pk)
            return render(request, 'edit_user.html',
                          {'form': form, 'message': 'Changes saved successfully', 'user_courses': user_courses})
    return render(request, 'edit_user.html', {'form': form, 'user_courses': user_courses})


def delete_user(request, pk):
    User.delete_user(pk)
    return redirect(reverse_lazy('index'))


def courses(request):
    return render(request, 'courses.html', {'courses': User.get_all_courses()})
