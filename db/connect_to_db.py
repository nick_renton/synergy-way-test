from django.conf import settings
import psycopg2


class ConnectDB(object):
    def __init__(self):
        self.conn = psycopg2.connect(settings.CONNECTION_STRING_TO_DB)
        self.cursor = self.conn.cursor()

    def __del__(self):
        self.cursor.close()
        self.conn.close()

    def execute_procedure(self, procedure, values):
        self.cursor.callproc(procedure, values)
        self.conn.commit()

    def select(self, sql):
        self.cursor.execute(sql)
        self.conn.commit
        list_users = tuple(self.cursor)
        return list_users

    def select_user(self, sql):
        self.cursor.execute(sql)
        self.conn.commit
        user_tuple = self.cursor.fetchone()
        user = dict([('id', user_tuple[0]), ('name', user_tuple[1]), ('email', user_tuple[2]),
                     ('phone', user_tuple[3]), ('mobile_phone', user_tuple[4]), ('status', user_tuple[5])])
        return user
