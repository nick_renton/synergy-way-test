import psycopg2

conn_string = "host='localhost' dbname='Users' user='postgres' password='manchester13'"

conn = psycopg2.connect(conn_string)

cursor = conn.cursor()

cursor.execute("""CREATE OR REPLACE FUNCTION add_user( name VARCHAR(100), email VARCHAR(40), phone VARCHAR(20),
 mobile_phone VARCHAR(20), status VARCHAR(7))
    RETURNS void AS $$
    BEGIN
      INSERT INTO core_user(user_id, name, email, phone, mobile_phone, status)
       VALUES (default, name, email, phone, mobile_phone, status);
    END;
    $$ LANGUAGE 'plpgsql';""")

cursor.execute("""CREATE OR REPLACE FUNCTION get_all_users()
    RETURNS TABLE( id integer, name varchar, email varchar, status varchar) AS
$BODY$
    select user_id, name, email, status from core_user;
$BODY$
      LANGUAGE sql;""")

cursor.execute("""CREATE OR REPLACE FUNCTION get_user( id integer)
    RETURNS TABLE( id integer, name varchar, email varchar, phone varchar, mobile_phone varchar, status varchar) AS
$BODY$
    select user_id, name, email, phone, mobile_phone, status FROM core_user WHERE user_id = id;
$BODY$
      LANGUAGE sql;""")

cursor.execute("""CREATE OR REPLACE FUNCTION edit_user(id integer, new_name VARCHAR(100), new_email CHAR(40), new_phone VARCHAR(20),
    new_mobile_phone VARCHAR(20), new_status VARCHAR(7))
    RETURNS void AS $$
    BEGIN
    UPDATE core_user SET name=new_name, email=new_email, phone=new_phone, mobile_phone=new_mobile_phone, status = new_status
    WHERE user_id = id;
    END;
    $$ LANGUAGE 'plpgsql'""")

cursor.execute("""CREATE OR REPLACE FUNCTION get_user_by_name( searched_name varchar)
    RETURNS TABLE( id integer, name varchar, email varchar, phone varchar, mobile_phone varchar, status varchar)
     AS $BODY$
    SELECT * FROM core_user WHERE name= searched_name;
    $BODY$
    LANGUAGE 'sql';""")

cursor.execute("""CREATE OR REPLACE FUNCTION delete_user( id integer)
    RETURNS void AS $$
    DELETE FROM core_user WHERE user_id = id;$$
    LANGUAGE sql;""")

cursor.execute("""CREATE OR REPLACE FUNCTION get_all_courses()
    RETURNS TABLE( course_code varchar, course_name varchar) AS
$BODY$
    SELECT code, course_name FROM core_courses;
$BODY$
      LANGUAGE sql;""")

cursor.execute("""CREATE OR REPLACE FUNCTION get_user_courses( pk integer)
    RETURNS TABLE(course_code VARCHAR, course_name VARCHAR) AS
$BODY$
    SELECT cc.code, cc.course_name FROM core_user_course  AS cuc JOIN core_courses AS cc ON cuc.course_id = cc.course_id
    WHERE cuc.user_id = pk;
$BODY$
      LANGUAGE sql;""")

cursor.execute("""CREATE OR REPLACE FUNCTION add_user_courses(user_id INTEGER, course_code VARCHAR )
    RETURNS void AS $$
    BEGIN
       INSERT INTO core_user_course (user_id, course_id )
       VALUES (user_id, (SELECT course_id FROM core_courses WHERE code = course_code));
       END;
    $$ LANGUAGE 'plpgsql';""")

cursor.execute("""CREATE OR REPLACE FUNCTION delete_course_for_user(current_user_id INTEGER, current_course_code VARCHAR)
    RETURNS void AS $$
    DELETE FROM core_user_course WHERE course_id = (SELECT course_id FROM core_courses
    WHERE code = current_course_code) AND user_id=current_user_id;$$
    LANGUAGE sql;""")

conn.commit()
cursor.close()
conn.close()
