import psycopg2

conn_string = "host='localhost' dbname='Users' user='postgres' password='manchester13'"

conn = psycopg2.connect(conn_string)

cursor = conn.cursor()

cursor.execute("""CREATE TABLE core_courses (course_id SERIAL NOT NULL PRIMARY KEY ,code VARCHAR(10) NOT NULL UNIQUE,
                course_name VARCHAR(30) NOT NULL);""")

cursor.execute("""INSERT INTO core_courses (code, course_name)
               VALUES ('P012345', 'Python_Base'),
                      ('P234567', 'Python-Database'),
                      ('H234678', 'HTML'),
                      ('J456789', 'Java-Base'),
                      ('JS543210', 'JavaScript-Base')""")

cursor.execute("""CREATE TABLE core_user (user_id serial NOT NULL PRIMARY KEY, name VARCHAR(100) NOT NULL,
 email VARCHAR(40) NOT NULL, phone VARCHAR(20),
  mobile_phone VARCHAR(20), status VARCHAR(8) NOT NULL);
;""")

cursor.execute("""CREATE TABLE core_user_course (id SERIAL PRIMARY KEY , user_id INTEGER NOT NULL REFERENCES core_user (user_id),
 course_id INTEGER NOT NULL REFERENCES core_courses (course_id));""")

conn.commit()
cursor.close()
conn.close()
