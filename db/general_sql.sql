CREATE DATABASE Users;

CREATE TABLE core_courses (
  course_id   SERIAL      NOT NULL PRIMARY KEY,
  code        VARCHAR(10) NOT NULL UNIQUE,
  course_name VARCHAR(30) NOT NULL
);

INSERT INTO core_courses (code, course_name)
VALUES ('P012345', 'Python_Base'),
  ('P234567', 'Python-Database'),
  ('H234678', 'HTML'),
  ('J456789', 'Java-Base'),
  ('JS543210', 'JavaScript-Base');

CREATE TABLE core_user (
  user_id      SERIAL       NOT NULL PRIMARY KEY,
  name         VARCHAR(100) NOT NULL,
  email        VARCHAR(40)  NOT NULL,
  phone        VARCHAR(20),
  mobile_phone VARCHAR(20),
  status       VARCHAR(8)   NOT NULL
);

CREATE TABLE core_user_course (
  id        SERIAL PRIMARY KEY,
  user_id   INTEGER NOT NULL REFERENCES core_user (user_id),
  course_id INTEGER NOT NULL REFERENCES core_courses (course_id)
);

CREATE OR REPLACE FUNCTION add_user(name         VARCHAR(100), email VARCHAR(40), phone VARCHAR(20),
                                    mobile_phone VARCHAR(20), status VARCHAR(8))
  RETURNS VOID AS $$
BEGIN
  INSERT INTO core_user (user_id, name, email, phone, mobile_phone, status)
  VALUES (DEFAULT, name, email, phone, mobile_phone, status);
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION get_all_users()
  RETURNS TABLE(id INTEGER, name VARCHAR, email VARCHAR, status VARCHAR) AS
$BODY$
SELECT
  user_id,
  name,
  email,
  status
FROM core_user;
$BODY$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION get_user(id INTEGER)
  RETURNS TABLE(id INTEGER, name VARCHAR, email VARCHAR, phone VARCHAR, mobile_phone VARCHAR, status VARCHAR) AS
$BODY$
SELECT
  user_id,
  name,
  email,
  phone,
  mobile_phone,
  status
FROM core_user
WHERE user_id = id;
$BODY$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION edit_user(id               INTEGER, new_name VARCHAR(100), new_email CHAR(40),
                                     new_phone        VARCHAR(20),
                                     new_mobile_phone VARCHAR(20), new_status VARCHAR(7))
  RETURNS VOID AS $$
BEGIN
  UPDATE core_user
  SET name = new_name, email = new_email, phone = new_phone, mobile_phone = new_mobile_phone, status = new_status
  WHERE user_id = id;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION get_user_by_name(searched_name VARCHAR)
  RETURNS TABLE(id INTEGER, name VARCHAR, email VARCHAR, phone VARCHAR, mobile_phone VARCHAR, status VARCHAR)
AS $BODY$
SELECT *
FROM core_user
WHERE name = searched_name;
$BODY$
LANGUAGE 'sql';

CREATE OR REPLACE FUNCTION delete_user(id INTEGER)
  RETURNS VOID AS $$
DELETE FROM core_user
WHERE user_id = id;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION get_all_courses()
  RETURNS TABLE(course_code VARCHAR, course_name VARCHAR) AS
$BODY$
SELECT
  code,
  course_name
FROM core_courses;
$BODY$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION get_user_courses(pk INTEGER)
  RETURNS TABLE(course_code VARCHAR, course_name VARCHAR) AS
$BODY$
SELECT
  cc.code,
  cc.course_name
FROM core_user_course AS cuc JOIN core_courses AS cc ON cuc.course_id = cc.course_id
WHERE cuc.user_id = pk;
$BODY$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION add_user_courses(user_id INTEGER, course_code VARCHAR)
  RETURNS VOID AS $$
BEGIN
  INSERT INTO core_user_course (user_id, course_id)
  VALUES (user_id, (SELECT course_id
                    FROM core_courses
                    WHERE code = course_code));
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION delete_course_for_user(current_user_id INTEGER, current_course_code VARCHAR)
  RETURNS VOID AS $$
DELETE FROM core_user_course
WHERE course_id = (SELECT course_id
                   FROM core_courses
                   WHERE code = current_course_code) AND user_id = current_user_id;$$
LANGUAGE SQL;