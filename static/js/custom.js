/**
 * Created by Mykola on 21.11.2016.
 */
$(document).ready(function () {
    $('#id_course').css('display', 'none');
    $('#id_phone').attr('pattern', '[\+]\d{2}[\(]\d{3}[\)]\d{7}');
    $('#id_mobile_phone').attr('pattern', '[\+]\d{2}[\(]\d{3}[\)]\d{7}');
});

$('#button-id-add').click(function () {
    if ($('#id_course').css('display') == 'none') {
        $('#id_course').css('display', 'block');
    } else {
        $('#div_id_course').append(add_course($('#id_course').val(), $('#id_course option:selected').text()));
        if ($(":input[name='selected_courses']").length == 5) {
            $('#id_course').css('display', 'none');
            $('#button-id-add').css('display', 'none');
        }
    }
});

function add_course(code, course) {
    $('#id_course  option[value="' + code + '"]').remove();
    return "<p id='" + code + "' class='custom-course'><input type='hidden'  name='selected_courses' value='" + code + "'>"
        + course + "  <a href='#'  class='remove_course'><span class='glyphicon glyphicon-remove-circle'></span></a></p>";
}

$('body').delegate('.remove_course', 'click', function () {
    var name_course = $(this).parent()[0].innerText;
    $('#id_course')
        .append($("<option></option>")
            .attr("value", $(this).parent()[0].id)
            .text(name_course));
    $(this).parent().remove();
    if ($('#id_course').css('display') == 'none') {
        $('#id_course').css('display', 'block');
        $('#button-id-add').css('display', 'block');
    }
});

